//Bài 1: in số từ 1-100

var contentHTML = "";
var rowNum = "";

for (var num = 1; num <= 100; num++) {
  rowNum += num + ", ";
  if (num % 10 == 0) {
    contentHTML += `<p>${rowNum}</p>`;
    rowNum = "";
  }
}

document.getElementById("result").innerHTML = contentHTML;
