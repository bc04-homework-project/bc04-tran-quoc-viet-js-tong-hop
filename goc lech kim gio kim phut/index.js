// Bài 10: Tính góc lệch giữa kim giờ và kim phút

function tinhGocLech() {
  let hour = document.getElementById("hour").value * 1;
  let min = document.getElementById("min").value * 1;

  let toaDoKimPhut = min * 6;
  let toaDoKimGio = hour * 30 + min * 0.5;
  let gocLech = 0;
  let gocLechTo = 0;

  toaDoKimGio > toaDoKimPhut
    ? (gocLech = toaDoKimGio - toaDoKimPhut)
    : (gocLech = toaDoKimPhut - toaDoKimGio);

  gocLech > 180 ? (gocLechTo = gocLech) : (gocLechTo = 360 - gocLech);
  let gocLechNho = 360 - gocLechTo;

  document.getElementById(
    "result"
  ).innerHTML = `Góc lệch giữa kim giờ và kim phút khi đồng hồ chỉ ${hour} : ${min} <br>
Góc lệch to = <span class="text-success">${gocLechTo}</span> độ <br>
Góc lệch nhỏ = <span class="text-danger">${gocLechNho}</span> độ 
`;
}
