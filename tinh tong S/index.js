// Bài 3: Tính tổng S

function sum() {
  let n = document.getElementById("num").value * 1;

  if (n <= 2) {
    document.getElementById(
      "result"
    ).innerHTML = `Tham số n phải > 2 để tính tổng`;
  } else {
    let s = 0;
    for (let i = 2; i <= n; i++) {
      s += i;
    }

    document.getElementById("result").innerHTML = s + n * 2;
  }
}
