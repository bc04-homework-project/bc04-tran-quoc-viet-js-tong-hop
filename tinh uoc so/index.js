// Bài 4: Tính số lượng ước của tham số

function tinhUoc() {
  let n = document.getElementById("num").value * 1;
  let nArr = [];

  if (n <= 0) {
    document.getElementById("result").innerHTML = `Tham số n phải > 0`;
  } else {
    for (let i = 1; i <= n; i++) {
      if (n % i == 0) {
        nArr.push(i);
      }
    }
    document.getElementById(
      "result"
    ).innerHTML = `Số <span class="text-danger">${n}</span> có <span class="text-primary">${nArr.length}</span> ước. Bao gồm : <span class="text-success">${nArr}</span>`;
  }
}
