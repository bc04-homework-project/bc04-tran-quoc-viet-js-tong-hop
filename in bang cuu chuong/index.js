//Bài 7: in bảng cửu chương

function inBangCuuChuong() {
  var n = document.getElementById("num").value * 1;

  var bangCuuChuong = "";

  for (var num = 0; num <= 10; num++) {
    bangCuuChuong += `<p>${n} x ${num} = ${n * num} </p>`;
  }

  document.getElementById("result").innerHTML = bangCuuChuong;
}
