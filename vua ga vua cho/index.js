// Bài 9: Tính số gà số chó

function countChickDog() {
  let tongSoCon = document.getElementById("dogChick").value * 1;
  let tongSoChan = document.getElementById("leg").value * 1;
  // console.log({tongSoCon, tongSoChan});

  if (tongSoCon <= 0) {
    document.getElementById(
      "result"
    ).innerHTML = `Bạn Híuuu à! Số con phải lớn hơn 0 mới có chân để đếm chớ!`;
  } else if (tongSoChan > tongSoCon * 4 || tongSoChan < tongSoCon * 2) {
    document.getElementById(
      "result"
    ).innerHTML = `Bạn Híuuu à! Số chân cần lớn hơn hoặc bằng tổng số con x 2 và bé hơn hoặc bằng tổng số con x 4`;
  } else if (tongSoChan % 2 != 0) {
    document.getElementById(
      "result"
    ).innerHTML = `Bạn Híuuu à! Số chân cần phải là số chẵn nhoa. Chứ mà đếm xong tự nhiên thấy có con nào bị cụt chân thì kỳ lắm!`;
  } else {
    var m = tongSoCon;
    var n = tongSoChan;
    var dog = 0;
    var chick = m - dog;
    for (; chick * 2 + dog * 4 < n; ) {
      dog++;
      chick = m - dog;
    }
    document.getElementById(
      "result"
    ).innerHTML = `Có <span class="text-danger">${chick}</span> con gà và <span class="text-success">${dog}</span> con chó . <br> Tìm được gòy mừng quó bạn Híu o !`;
  }
}
