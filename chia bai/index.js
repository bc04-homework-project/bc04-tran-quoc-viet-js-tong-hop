//Bài 8: Chia bài cho 4 người chơi

var players = [[], [], [], []];

var cards = [
  "4K",
  "KH",
  "5C",
  "KA",
  "QH",
  "KD",
  "2H",
  "10S",
  "AS",
  "7H",
  "9K",
  "10D",
];

for (var i = 0; cards.length > 0; i++) {
  // console.log('i', i)
  // console.log('cards.length: ', cards.length);

  for (var n = 0; n < players.length; n++) {
    players[n].push(cards.shift());

    // console.log('cards1: ', cards);
    // console.log('players1: ', players);
  }
}
// console.log('cards2: ', cards);
// console.log('players2: ', players);

var result = "";
for (var e = 0; e < players.length; e++) {
  result += `<p>Player ${e + 1} : ${players[e]}</p>`;
}

document.getElementById("result").innerHTML = result;
