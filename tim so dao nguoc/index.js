// Bài 5: Tìm số đảo ngược

function timSoDaoNguoc() {
  var n = document.getElementById("num").value;

  let revertNum = "";

  for (var i = n.length - 1; i >= 0; i--) {
    revertNum += n[i];
  }

  document.getElementById("result").innerHTML = revertNum;
}
